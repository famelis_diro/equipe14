package gymreal;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class static contenant les methodes necessaire a la gestion 
 * des inscriptions et a leur manipulation
 */

public class Inscription {
	
	//Entité responsable pour la gestion du fichier contenant les confirmations
	private static Csvio csv = new Csvio("inscriptions.csv");
	
	
	/**
	 * Fonction permettant de créer un nouvel enregistrement
	 * d'une inscription dans un fichier
	 * */
	public static void creation() {
		
		//Saisie de l'information necessaire pour verifier si il est possible de 
		//creer une confirmation
		String id = Console.input("Veuillez entrer votre ID de membre");
		String service = Console.input("Veuillez entrer le numero du service");
		int ligne = Service.rechercher(Integer.parseInt(service));
		String date  = Console.input("Veuillez saisir la date ou le service sera fournit (JJ-MM-AAAA)");
	
		//Pour formatter la date actuel
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		
		//Verification et saisie de l'information supplementaire pour creer la confirmation
		if( Membre.valider(Integer.parseInt(id),false) && ligne != -1
				&& !Inscription.existe(id, service, date)) {
			
			//Obtenir l'infode du service relié
			String[] info = Service.getLine(ligne).split(",");
			String[] tab= new String[6];
			
			//Preparation du tableau pour l'enregistrement
			tab[0] = dtf.format(LocalDateTime.now()); //JJ-MM-AAAA HH:MM:SS
			tab[1] = date;
			tab[2] = info[7];
			tab[3] = id;
			tab[4] = info[10];
			tab[5] = Console.input("Entrer un commentaire relatif a cette inscription");
			
			//Enregistrement sur le fichier
			csv.ajouter(tab);
	
			//fin de la sequence
			Console.output("Le cout de la seance sera de: " + Service.ajustePrix(info[8]) + "\n");
		}
		else {
	        Console.output("L'enregistrement n'est pas créé");
			}
		
		return;
		
	}
	
	/**
	 * Fonction permettant de confirmer si un enregistrement 
	 * d'une inscription existe dans le fichier en donnant le 
	 * membre inscrit et le code de service et la date pour la seance
	 * */
	public static boolean existe(String membre, String service, String date) {
		String [] id = {membre, service, date};//Id du membre, le code de service, date de la seance
		int [] champs = {3,4,1};//Index ou se trouve les trois champs recherchés
		
		//rechercher renvoie la ligne de ou ae trouve l'enregistrement dans le fichier
		if (csv.rechercher(id, champs) != -1) return true;
		
		return false;
	}
	
	
	/**
	 * Fonction permettant de d'afficher 
	 * combien d'inscription son faite pour une seance
	 * */
	public static void afficher() {
		//Saisie de l<information
		String prof    = Console.input("Veuillez rentrer le numero du professionel");
		String service = Console.input("Veuillez entrer le code du service");
		String date    = Console.input("Veuillez entrer la date a verifier desire");
		
		//Obtenir tout les inscriptions
		List<String> aAfficher = Inscription.getall(prof, service, date);
		
		//Affiche les resultats
		Console.output("Nombre d'inscription pour le service:" + service + 
				       "Donne le" + date + ": " + aAfficher.size() + "\n");
	}
	
	
	/**
	 * Fonction permettant de d'obtennir tout les inscriptions
	 * pour un service donne par un professionel a une date donne
	 * */
	private static List<String> getall(String prof, String service, String date){
		
		String[] aTrouver = {prof, service, date};//Id prof., code de service, date de la seance
		int[] indexChamps = {2,4,1};//Index ou se trouve les trois champs recherchés
				
		List<String> inscriptions = csv.getAll(aTrouver, indexChamps);
		
		
		return inscriptions;
	}

}
