package gymreal;

/**
 * Class static contenant les methodes necessaire a la gestion 
 * des professionnels et a leur manipulation
 */
public class Professionnel extends Affilie {

	//Entité responsable pour la gestion du fichier contenant les confirmations
    private static Csvio csv = new Csvio("professionnels.csv");

	/**
	 * Methode de creation de l'enregistrement d'un nouveau professionel
	 */
    public static void creation() {
        Affilie.creation(csv);
    }

	/**
	 * Methode contenant la logique pour les
	 * different acces possible au profil dun profesionnel 
	 */
    public static void accesProfil() {
        int choix = Integer.parseInt(Console.input("Acceder au profil par ID de professionnel (1) ou par nom (2) ?", true));
        if (choix == 1) {
            int id = Integer.parseInt(Console.input("Veuillez entrer le ID de professionnel a 9 chiffres", true));
            int index = csv.rechercher(id,6); //retourne la ligne du fichier ou se trouve le membre, -1 si il n'y est pas
            if (index == -1) {
            	Console.output("Mauvais numero de professionnel");
            	return;
            }
            choix = Integer.parseInt(Console.input("Voulez-vous modifier (1) ou supprimer (2) le profil?", true));
            if (choix == 1) {
                modification(index, csv);
            }
            else if (choix == 2) {
                suppression(index, csv);
            }
        }
        else if (choix == 2) {
            String nom = Console.input("Veuillez entrer le nom du professionnel.");
            if (csv.rechercheIDParString(nom,0) == -1) //retourne la ligne du fichier ou se trouve le membre, -1 si il n'y est pas 
            {
                Console.output("Nom introuvable");
            }
            else {
                int index = csv.rechercheIndexParString(nom,0);
                choix = Integer.parseInt(Console.input("Voulez-vous modifier (1) ou supprimer (2) le profil du professionnel?", true));
                if (choix == 1) {
                    modification(index, csv);
                }
                else if (choix == 2) {
                    suppression(index, csv);
                }
            }
        }
    }

}
