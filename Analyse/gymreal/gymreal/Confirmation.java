package gymreal;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Class static contenant les methodes necessaire a la gestion 
 * des confirmations et a leur manipulation
 */

public class Confirmation {

	//Entité responsable pour la gestion du fichier contenant les confirmations
	private static Csvio csv= new Csvio("confirmations.csv");
	
	
	
	/**
	 * Fonction permettant de créer un nouvel enregistrement
	 * d'une confirmation dans un fichier
	 * */
	public static void creation() {
		
		//Saisie de l'information necessaire pour verifier si il est possible de 
		//creer une confirmation
		String id = Console.input("Veuillez entrer votre ID de membre", true);
		String service = Console.input("Veuillez entrer le numero du service", true);
		int ligne = Service.rechercher(Integer.parseInt(service));
		
		//Obtennir la date actuel et la formatter
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm:ss");
		dtf.format(LocalDateTime.now()); //JJ-MM-AAAA HH:MM:SS

		//Verification et saisie de l'information supplementaire pour creer la confirmation
		if(Membre.valider(Integer.parseInt(id),false) && ligne != -1
				&& !Confirmation.existe(id, service)) {
			String[] info = Service.getLine(ligne).split(",");
			String[] tab= new String[5];
			
			tab[0] = dtf.format(LocalDateTime.now()); //JJ-MM-AAAA HH:MM:SS
			tab[1] = info[7];		//professionnel
			tab[2] = id;
			tab[3] = service;
			tab[4] = Console.input("Entrer un commentaire relatif a cette inscription");
			
			//Enregistrement sur le fichier
			csv.ajouter(tab);
			
		}
		else {
	        Console.output("L'enregistrement n'est pas créé");
			}
		
		return;
		
	}
	
	
	/**
	 * Fonction permettant de confirmer si un enregistrement 
	 * d'une confirmation existe dans le fichier en donnant le 
	 * membre inscrit et le code de service
	 * */
	public static boolean existe(String membre, String service) {
		
		String [] id = {membre, service}; //Id du membre et le code de service
		int [] champs = {3,4}; //Index ou se trouve les deux champs rechercher
		
		//Permet d'obtennir la ligne ou se trouve l'enregistrement recherche 
		int ligne = csv.rechercher(id, champs);
		
		if (ligne != -1) {
			
			//Obtennir la date de l'enregistrement
			String entree = csv.records.get(ligne);
			String[] tab= entree.split(",");
			LocalDateTime temps= OutilsTime.convertDateTime(tab[0]);
			
			//verifie que la confirmation est faite le bon jour
			if ((temps.getDayOfYear())==(LocalDateTime.now().getDayOfYear()))
				return true;
			
		}
		
		return false;
	}
	
	
	/**
	 * Fonction permettant d'obtennir tous les enregistrements 
	 * de confirmation correspondant a deux paraemetre donne 
	 * 
	 * */
	private static List<String> getall(String prof, String service){
		
		String[] aTrouver = {prof, service}; //Id du professionnelle et du code de service
		int[] indexChamps = {2,4}; //Index se trouve les deux champs rechercher
		
		//retourne tous les enregistrement 
		List<String> confirmations = csv.getAll(aTrouver, indexChamps);
		
		
		
		return confirmations;
	}
	
	public static void getTef() {	//méthode non-implémentée de creation des TEF
		
		Console.output("Les TEF ont bien été créés");
	}
	
	public static void getRapport() {	//méthode non-implémentée de création de rapport

		Console.output("Le rapport de synthèse a été envoyé.");
	}
	

}
