package gymreal;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Class permettant la gestion d'un fichier de type csv
 */
public class Csvio {

	private BufferedReader fr;
	private BufferedWriter fw;
	private PrintWriter clear;
	private File fichier ;
	//Enregistrement de manipuler plus aisement le contenue d'un fichier
	public List<String> records; 
	
	
	/**
	 * Constructeur de la classe, nécéssite uniquement le path du fichier qui sera créé
	 */
	public Csvio(String path) {
		try {
			
			this.fichier = new File(path);
			this.fichier.createNewFile();
			this.fr = new BufferedReader(new FileReader(fichier));
			this.records = this.getRecords(); //Met le fichier dans une liste pour le manipuler plus aisement
			this.fichier.setWritable(true);
			FileWriter writer = new FileWriter(fichier,true);
			this.fw = new BufferedWriter(writer);
		}
		catch(Exception e) {
			System.out.println("Exception IO, erreur a la construction du file handler: " + e);
		}
		finally {}
	}
	
	
	/**
	 * Methode pour lire le fichier et le mettre dans une liste de string
	 */
	public List<String> getRecords(){
		
		List<String> entrees = new ArrayList<String>();
		try {
			String ligne= fr.readLine();
			while(ligne != null && ligne.startsWith("")) {
				entrees.add(ligne);
				ligne = fr.readLine();
			}
			
			return entrees;
		}
		
		catch(Exception e) {
			Console.output("Exception IO, can't get the reccords");
			return entrees;
		}
	}
	
	
	/**
	 * Methode qui trouve le plus grand id déjà attribué dans 
	 * le fichier et retourne ensuite le prochain id a utiliser
	 * l'index represente la valeur qu'il faut regarder sur chaque ligne
	 */
	public int findId(int index) 
	{
		int id =0;
		for(int i=0; i<this.records.size();i++) {
			String[] ligne= readLine(i);
			int nextid =Integer.parseInt(ligne[index]);
			if (nextid > id) id = nextid;
		}
		return id+1;
	}
	
	/**
	 * Methode qui retourne les differents elements trouvees sur une
	 * ligne du fichier
	 */
	private String[] readLine(int indexLigne) {
		
		String ligne = this.records.get(indexLigne);	
		return ligne.split(",");

	}
	
	
	/**
	 * Methode qui ajoute un enregistrement dans le fichier
	 */
	public void ajouter(String[] tabChamps) {
		records.add(String.join(",", tabChamps));
		//Une fois l'enregistrement ajoute dans la liste de l'instance
		//on s'assure que le ficier est les memes valeurs
		updateFile();
	}
	
	
	/**
	 * Methode qui ajoute une valeur d'un enregistrement,
	 * i represente la ligne dans le fichier a changer,
	 * la colone indique qu'elle champs changer pour l'enregistrement
	 * et s la nouvelle valeur que le champs aura
	 */
	public void changer(String s, int colonne, int i) {
		
		//Logique pour aller chercher l'enregistrement et modifier le champs
		String [] ligne= readLine(i);
		ligne[colonne] = s;
		records.set(i, String.join(",", ligne));
		
		//Une fois l'enregistrement ajoute dans la liste de l'instance
		//on s'assure que le ficier est les memes valeurs
		updateFile();
		
		return;
	}
	
	
	/**
	 * Methode qui supprime un enregistrement du fichier,
	 * en passant en parametre la ligne du fichier a effacer
	 */
	public void supprimer(int indexLigne) {
		
		try {
			//Enleve de la liste l'enregistrement
			records.remove(indexLigne);
			
			//Une fois l'enregistrement ajoute dans la liste de l'instance
			//on s'assure que le ficier est les memes valeurs
			updateFile();
		}
		catch(Exception e) {
			Console.output("io excpetion");
		}
		
	}
	
	
	/**
	 * Methode qui recopie les valeurs de la listes d'enregistrement 
	 * dans le fichier
	 */
	private void updateFile() {
		try {
			this.clear = new PrintWriter(this.fichier); //efface le contenue du fichier
			this.clear.close();
			//ecrit tout les lignes de la liste dans le fichier en faisant un append
			for(int i = 0; i<records.size(); i++) {	
				this.fw.write(records.get(i)+"\n");
				//Implementer une sauvegarde des ligne non copie en cas d'echec pour re-essayer 
			}
			this.fw.flush();
			Console.output("Enregistrement réussi\n");
			
		}
		catch(Exception e) {
			Console.output("Exception IO");
		}
		
	}
	
	
	/**
	 * Methode qui retourne tout les enregistrements qui on une valeur donne 
	 * au champs signaler par indexchamps
	 */
	public List<String> getAll(String aTrouver, int indexChamps){
		
		List<String> entrees = new ArrayList<String>();
		
		try {
			String ligne= fr.readLine();
			while(ligne != null && ligne.startsWith("")) {
				
				String ligneSplit = ligne;
				if (ligneSplit.split(",")[indexChamps] == aTrouver) 
					entrees.add(ligne);
				
				ligne = fr.readLine();
				
			}
			
			return entrees;
		}
		
		catch(Exception e) {
			Console.output("Exception IO");
			return entrees;
		}
	}
	
	/**
	 * Methode qui retourne tout les enregistrements qui on les valeurs donnes 
	 * aux champs donnés par indexchamps
	 */
	public List<String> getAll(String[] aTrouver, int[] indexChamps){
		
		List<String> entrees = new ArrayList<String>();
			int next = -1;
			int index;
			do {
				index = rechercher(aTrouver, indexChamps,next);
				if (index != -1) entrees.add(records.get(index));
				next = index;
						
			} while (index != -1);
		
			return entrees;
	}

	
/*---------------------------------------------------------------------------*/
/*Methode de recherche dans les fichiers, menage a faire */
	
	public int rechercher(int id, int champs) {
		
		for(int i=0; i<records.size(); i++) {
			String[] ligne= readLine(i);
			if (Integer.parseInt(ligne[champs])==id) return i;
			
		}
		return -1;
	}
	
	public int rechercher(String[] id, int[] champs) {
		
		for(int i=0; i<records.size(); i++) {
			String[] ligne= readLine(i);
			int checks = 0;
			for (int j=0; j<id.length; j++) {
			if( id[j].equals(ligne[champs[j]]) ) {checks++; continue;}
			break;
			}
			if (checks == id.length) return i;
		}
		return -1;
	}
	
	public int rechercher(String[] id, int[] champs, int next) {
		
		for(int i= next+1; i<records.size(); i++) {
			String[] ligne= readLine(i);
			int checks = 0;
			for (int j=0; j<id.length; j++) {
				if( id[j].equals(ligne[champs[j]]) ) {checks++; continue;}
				break;
			}
			if (checks == id.length) return i;
		}
		return -1;
	}
	

	public int rechercheIDParString(String nom, int champs) {
		for (int i = 0; i < records.size(); i++) {
			String[] ligne = readLine(i);
			if (ligne[champs].equals(nom)) return Integer.parseInt(ligne[6]);
		}
		return -1;
	}
	
	public int rechercheIndexParString(String nom, int champs) {
		for (int i = 0; i < records.size(); i++) {
			String[] ligne = readLine(i);
			if (ligne[champs].equals(nom)) return i;
		}
		return -1;
	}
}
