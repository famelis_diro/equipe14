package gymreal;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.List;
import java.time.*;
import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.YEAR;



public class Service {
	
	//Entité responsable pour la gestion du fichier contenant les confirmations
	private static Csvio csv = new Csvio("seances.csv");
	
	//Contient le prochain code de service qui sera assigne
	private static int codeService = csv.findId(10);
	
	//Format utilise pour representer les dates
	private DateTimeFormatter format = new 
			DateTimeFormatterBuilder().appendValue(DAY_OF_MONTH,2).appendLiteral("-").appendValue(MONTH_OF_YEAR,2)
			.appendLiteral("-").appendValue(YEAR,4).toFormatter();
	
	
	/**
	 * Fonction permettant de créer un nouvel enregistrement
	 * d'un service dans un fichier
	 * */
	public static void creation() {
		
		//Saisie de l'information necessaire pour créer le service
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MMM/dd HH:mm:ss");
		String[] tab= new String[11];
		tab[0] = dtf.format(LocalDateTime.now()); //JJ-MM-AAAA HH:MM:SS
		tab[1] = Console.input("Date du début du service (JJ-MM-AAAA)");
		tab[2] = Console.input("Date de fin du service (JJ-MM-AAAA)");
		tab[3] = Console.input("Nom du service");
		tab[4] = Console.input("Heure de la séance (HH:MM)");
		tab[5] = Console.input("Jour de la séance? Lundi (1), mardi (2)... (1-3-...-6)");
		tab[6] = Console.input("Capacité maximale");
		tab[7] = Console.input("Numéro du professionnel");
		tab[8] = Console.input("Frais du service");
		tab[9] = Console.input("Commentaires");
		tab[10]= String.format("%07d", codeService);
		codeService= csv.findId(10);
		
		//Enregistrement du service
		csv.ajouter(tab);
		
		return ;
		
	}
	
	/**
	 * Recherche un service selon son code et renvoie la ligne du fichier ou
	 * il se trouve
	 */
	public static int rechercher(int id) {	
		return csv.rechercher(id,10);
	}
	
	/**
	 * Renvoie la ligne du fichier demander
	 */
	public static String getLine(int ligne) {
		return csv.records.get(ligne);
	}
	
	/**
	 * Methode contenant la logique pour modifer un service 
	 * contenu a ligne passé en parametre
	 */
	public static void modifier(int ligne) {
				
		String rep = Console.input("Voulez-vous changer le local ou le coût du service? (oui : 1, non: 2)");
		
		if (rep.equals("1")) {
			Console.output("Impossible");
			return;
		}
		
		String rep2;
		do {
		//Affichage des options possibles a modifie
		rep2 = Console.input("Voulez-vous changer (1) la date de début du service\n"
				+ "(2) la date de fin du service\n"
				+ "(3) l'heure du service\n"
				+ "(4) la récurrence hebdomadaire\n"
				+ "(5) les commentaires\n"
				+ "OU (6) quitter ");
		
		
		//Traitement respectif pour changer le champ selectionné
		switch(rep2) {
		case "1": 
			String dateDeb =Console.input("Date du début du service (JJ-MM-AAAA)");
			modif(dateDeb, ligne, 1);
			break;
		
		case "2":
			String dateFin =Console.input("Date de fin du service (JJ-MM-AAAA)");
			modif(dateFin, ligne, 2);
			break;
		case "3":
			String heureSeance =Console.input("Heure de la séance (HH:MM)");
			modif(heureSeance, ligne, 4);
			break;
		case "4":
			String recurrence = Console.input("Jour de la séance? Lundi (1), mardi (2)... (1-3-...-6)");
			modif(recurrence, ligne, 5);
			break;
		case "5":
			String commentaires =Console.input("Commentaires");
			modif(commentaires, ligne, 9);
			break;
		default: Console.output("Argument non valide"); break; // erreur d'entrée

		}

		} while(rep2.equals("6"));
	}
		
	
	/**
	 * Methode d'envoie du parametre a modifier
	 */
	private static void modif(String newInfo, int indexLigne, int indexChamp) {		
		csv.changer(newInfo, indexChamp, indexLigne);
		/*Membre.aviser(); */
	}
	
	/**
	 * Suppression d'un service a une ligne donne du fichier
	 */
	public static  void supprimer(int indexLigne) {
		csv.supprimer(indexLigne);	
		/*Membre.aviser(); */
	}
	
	/**
	 * Methode utiliser pour afficher le repertoire 
	 * de service par 7 jours a partire du jour donne
	 */
	public static void afficher() {
		
		//Obtention de l'information relative au moment present
		//pour comparaison futur
		LocalDate nowDate = LocalDate.now();
		LocalDate staticNowDate = nowDate;
		int nowJour = LocalDate.now().getDayOfWeek().getValue();
		LocalTime nowHeure = LocalTime.now();
		
		//Contient le string afficher pour la semaine 
		String s = "";
		
		List<String[]> seances = new ArrayList<String[]>();
		
		do {
			
			for(int i=0; i<7; i++) {
				//Obtient tout les seances pour une journee
				seances = seancesJour(nowDate.plusDays(i), (nowJour+i)%7, nowHeure, staticNowDate);
				//Trie les seances obtenue en ordre croisant
				seances = trierSeances(seances);
				//Affiche les seance du jours
				afficherSeances(seances, nowDate.plusDays(i));
			}
			
			//Preparation pour la prochaine semaine
			nowDate=nowDate.plusDays(7);
			nowJour = (nowJour+7)%7; //Pas l'equivalent d'additionner zero??
			
			s = Console.input("Semaine suivante? Oui (1) ou non (2)");
			
		} while(s.equals("1"));
		
		
		
		
		return;
	}
	
	/**
	 * Methode pour obtennir tous les seances donne pour un jour donne
	 */
	private static List<String[]> seancesJour(LocalDate date, int jour, LocalTime heure, LocalDate now){
		
		List<String> seances = csv.records;
		List<String[]> bonnesSeances = new ArrayList<String[]>();
		
		//Verifie tout les seances pour voir si elles sont donne pour cette journee
		for(int i=0; i< seances.size();i++) {
			String[] entree = seances.get(i).split(",");
			LocalDate dateDeb= OutilsTime.convertDate(entree[1]);
			LocalDate dateFin = OutilsTime.convertDate(entree[2]);
			
			
			//Verifie si une recurence du service se trouve durant la journee passe en parametre
			
			if (date.isAfter(dateDeb) && date.isBefore(dateFin))//Borne des dates 
			{
				String[] recurrence = entree[5].split("-");
				
				for(int j=0; j<recurrence.length; j++) {					//jour
					
					if(Integer.parseInt(recurrence[j]) == jour) //Verifie pour le jour de la semaine
					{	
						if(OutilsTime.convertTime(entree[4]).isAfter(heure)) //Verifie pour l'heure 
						{	
							bonnesSeances.add(entree);
						}
						
						else if (date.isAfter(now)) {
							bonnesSeances.add(entree);
						}
					}
				}
			}
		}
		
		return bonnesSeances;
	}
	
	
	/**
	 * Trie les seances d'une liste en orde croisant selon leur date
	 */
	private static List<String[]> trierSeances(List<String[]> seances){

		String[] temp;
		for(int i=0; i < seances.size(); i++){  
			for(int j=1; j < (seances.size()-i); j++){  
				if(OutilsTime.convertTime(seances.get(j)[4]).isAfter(OutilsTime.convertTime(seances.get(j-1)[4]))){  
					//swap elements  
					temp = seances.get(j-1);  
					seances.set(j-1, seances.get(j));  
					seances.set(j, temp);  
				}  
			}  
		}  	
		return seances;
	}
	
	
	/**
	 * Affiche les seances d'une liste
	 */
	private static void afficherSeances(List<String[]> seances, LocalDate date) {
		
		int i=0;
		do {
			System.out.println(date.toString()+"\n");

			if (seances.isEmpty())
				Console.output("Aucune seance pour ce jour\n");
			
			//Information pertinente a afficher pour un membre pour une seance
			else {
				Console.output("Nom de la séance: "+ seances.get(i)[3]);
				Console.output("Heure de la séance: "+ seances.get(i)[4]);
				Console.output("Frais du service: "+ ajustePrix(seances.get(i)[8]));
				Console.output("Numero de seance: "+ seances.get(i)[10]);
				Console.output("Commentaires: "+ seances.get(i)[9]+"\n");
			}
			
			i++;
		} while(i < seances.size());
	}
	

	/**
	 * Renvoie le prix a payer pour une seance en ajustant 
	 * le prix que le professionnel charge au gym pour donner ca seance
	 */
	public static String ajustePrix(String frais) {
		double cout = Double.parseDouble(frais);
		cout = cout*1.15;
		return Double.toString(cout);
	}
	
}
