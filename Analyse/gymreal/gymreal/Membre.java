package gymreal;


/**
 * Class static contenant les methodes necessaire a la gestion 
 * des membres et a leur manipulation
 */
public class Membre extends Affilie {

	//Entité responsable pour la gestion du fichier contenant les confirmations
	private static Csvio csv = new Csvio("membres.csv");


	/**
	 * Methode de creation de l'enregistrement d'un nouveau membre
	 */
	public static void creation() {
	    Affilie.creation(csv);
    }

	/**
	 * Methode contenant la logique pour les
	 * different acces possible au profil d'un membre
	 */
    public static void accesProfil() {
    	
        int choix = Integer.parseInt(Console.input("Acceder au profil par ID de membre (1) ou par nom (2) ?", true));
        if (choix == 1) {
        	//Saisie du numero d'identifiaction
            int id = Integer.parseInt(Console.input("Veuillez entrer le ID de membre a 9 chiffres", true));
            int index = csv.rechercher(id,6); //retourne la ligne du fichier ou se trouve le membre, -1 si il n'y est pas
            if (index == -1) {
            	Console.output("Mauvais numero de membre");
            	return;
            }
            choix = Integer.parseInt(Console.input("Voulez-vous modifier (1) ou supprimer (2) le profil?", true));
            if (choix == 1) {
                modification(index, csv);
            }
            else if (choix == 2) {
                suppression(index, csv);
            }
        }
        
        //Recherche par nom
        else if (choix == 2) {
            String nom = Console.input("Veuillez entrer le nom du membre");
            if (csv.rechercheIDParString(nom,0) == -1) //retourne la ligne du fichier ou se trouve le membre, -1 si il n'y est pas 
            {
                Console.output("Nom introuvable");
            }
            else {
                int index = csv.rechercheIndexParString(nom,0);
                choix = Integer.parseInt(Console.input("Voulez-vous modifier (1) ou supprimer (2) le profil?", true));
                if (choix == 1) {
                    modification(index, csv);
                }
                else if (choix == 2) {
                    suppression(index, csv);
                }
            }
        }
    }
    
    
	/**
	 * Mehtode pour valider un membre selon son numero d'identification
	 */
	public static boolean valider(int id, boolean afficher) {
		
		//Valide que le membre existe
		int membre = csv.rechercher(id, 6);
		
		boolean statut = false;
		if (membre != -1) {
			String valid = csv.records.get(membre).split(",")[5];
			
			//affiche le statue du membre selon la valeur entree 
			switch(valid) {
				case "-2": if (afficher) Console.output("Membre banni pour autre raison que frais impaye"); break;
				case "-1": if (afficher) Console.output("Membre banni pour frais impaye"); break;
				case "1" : if (afficher) Console.output("Membre Valide"); statut =true; break;
				default  : if (afficher) Console.output("Statut du membre indetermine");
			}
		}
		
		return statut;
	}
	
	
}