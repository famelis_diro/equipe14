package gymreal;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class OutilsTime {

	public static LocalDate convertDate(String date) {
		int dateJour= Integer.parseInt(date.substring(0, 2));
		int dateMois=Integer.parseInt(date.substring(3,5));
		int dateAn=Integer.parseInt(date.substring(6,10));
		
		return LocalDate.of(dateAn, dateMois, dateJour);

	}
	
	public static LocalTime convertTime(String temps) {
		int heures=Integer.parseInt(temps.substring(0, 2));
		int minutes=Integer.parseInt(temps.substring(3,5));
		return LocalTime.of(heures, minutes);
	}
	
	public static LocalDateTime convertDateTime(String date) {
		int dateJour= Integer.parseInt(date.substring(0, 2));
		int dateMois=Integer.parseInt(date.substring(3,5));
		int dateAn=Integer.parseInt(date.substring(6,10));
		int timeHeure=Integer.parseInt(date.substring(11,13));
		int timeMin=Integer.parseInt(date.substring(14,16));
		int timeSec=Integer.parseInt(date.substring(17,19));

		
		return LocalDateTime.of(dateAn, dateMois, dateJour, timeHeure, timeMin, timeSec);

	}
}
