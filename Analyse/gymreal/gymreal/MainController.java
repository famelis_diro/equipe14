package gymreal;

/**
 * Class principal contenant la logique du logiciel,
 * Represente conceptuellement le gym
 */
public class MainController {
	
	
	/**
	 * Main du programme, represente le menu principal
	 */
    public static void main(String[] args) {
        while(true) {
        	Console.clearScreen();
        	//affiche les option accesible sur le menu principal
            Console.output(printMainOptions());
            int entree;
            
            do {
            	entree = Integer.parseInt(Console.input("Veuillez entrer un chiffre entre 1 et 8.",true));
            } while (entree < 1 || entree > 8);
            
            switch (entree) {
                case 1: MainController.gestionProfils(); break;
                case 2: MainController.gestionSeances(); break;
                case 3: MainController.inscriptionSeances(); break;
                case 4: MainController.statutMembres(); break;
                case 5: MainController.rapportSynthese(); break;
                case 6: MainController.procedureComptable(); break;
                case 7: MainController.repertoireService(); break;
                case 8: System.exit(0);
                default : Console.output("Invalid argument");
            }
        }
    }
	
	/**
	 * Renvoie le String contenant les options du menu principal
	 */
    private static String printMainOptions() {
        return ("Bienvenue, veuillez choisir parmi les options suivantes en écrivant le chiffre approprié \n" +
                "1 : Gestion des profils \n" +
                "2 : Gestion des séances \n" +
                "3 : Consultation des inscriptions aux séances \n" +
                "4 : Obtennir statut d'un membre \n" +
                "5 : Rapport de synthèse \n" +
                "6 : Procédure comptable \n" +
                "7 : Obtennir le repertoire de service \n" +
                "8 : Quitter le programme");
    }

    
	/**
	 * Methode contenant la logique pour faire la gestion d'un profil
	 * et appel les fonctions de membre ou professionnelle en consequence
	 */
    static void gestionProfils() {
        int status = Integer.parseInt(Console.input("Etes vous un membre (1) ou un professionnel (2)?", true));
        if (status == 1) {
            int choix = Integer.parseInt(Console.input("Voulez-vous creer un profil (1) ou acceder (2) a un profil existant", true));
            if (choix == 1) {
                Membre.creation();
            }
            else if (choix == 2) {
                Membre.accesProfil();
            }
        }
        else if (status == 2) {
            int choix = Integer.parseInt(Console.input("Voulez-vous creer un profil (1) ou acceder (2) a un profil existant", true));
            if (choix == 1) {
                Professionnel.creation();
            }
            else if (choix == 2) {
                Professionnel.accesProfil();
            }
        }
    }
    
	/**
	 * Methode contenant la logique pour faire la gestion d'une seance
	 * et appel les fonctions de service en consequence
	 */
    static void gestionSeances() {
        int option = Integer.parseInt(Console.input("Créer (1), modifier (2) ou supprimer (3)?",true));
        if (option == 1) {
            Service.creation();
        }
        else if (option == 2) {
            String id = Console.input("Veuillez entrer le ID de la séance",true);
            int numLigne =Service.rechercher(Integer.parseInt(id));
            
            if (numLigne!=-1)
                Service.modifier(numLigne);
            return;
        }
        else if (option == 3) {
            String id = Console.input("Veuillez entrer le ID de la séance",true);
            
            int numLigneSupp= Service.rechercher(Integer.parseInt(id));
            
            if (numLigneSupp != -1) 
            	Service.supprimer(numLigneSupp);

            return;
        }
        else {
            Console.output("Le numéro entré n'est pas valide.");
            return;
        }
    }
    
    /**
     * Methode pour appeler la methode de creation d'une inscription
     */
    static void inscriptionSeances() {
    	Inscription.creation();
    }
    
    /**
     * Methode pour appeler la methode de creation d'une confirmation
     */
    static void confirmation() {
        Confirmation.creation();
    }
    
    /**
     * Methode pour verifier le statut d'un membre
     */
    static void statutMembres() {
        int id = Integer.parseInt(Console.input("Veuillez entrer le user ID",true));
        Membre.valider(id, true);
    }

    
    /**
     * Methode pour obtennir le rapport de synthese
     */
    static void rapportSynthese() {
        Confirmation.getRapport();
    }
    
    
    /**
     * Methode pour envoyer le TEF
     */
    static void procedureComptable() {
    	Confirmation.getTef(); 	
    }
    
    /**
     * Methode pour afficher le repertoire de fichier
     */
    static void repertoireService() {
        Service.afficher();
        String s = Console.input("Voulez vous vous inscrire, confirmer ou quitter (1, 2 ou 3)",true);
        if (s.equals("1"))
            inscriptionSeances();
        else if (s.equals("2"))
            confirmation();
        else
            return;
    }
    
}
