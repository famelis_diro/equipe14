package gymreal;


/**
 * Class abstraite contenant les methode communes pour la gestion 
 * des membres et des professionnel
 */
abstract class Affilie {

	/**
	 * Fonction permettant de créer un nouvel enregistrement
	 * d'un membre ou d'un professionel dans un fichier
	 * en passant l'instance responsable de la gestion 
	 * du fichier en parametre
	 * */
    public static void creation(Csvio csv) {
    	//séquence d'information demandé
        String[] tab = new String[7];
        tab[0] = Console.input("Nom");
        tab[1] = Console.input("Date de naissance");
        tab[2] = Console.input("Adresse courriel");
        tab[3] = Console.input("Adresse postale");
        tab[4] = Console.input("Numero de telephone");
        tab[5] = "1"; //Statut, 1 ok, -1 frais non-paye, -2 banni autre raison
        tab[6] = String.valueOf(csv.findId(6));
        
        //création de l'enregistrement dans le fichier 
        csv.ajouter(tab);
    }

	/**
	 * Fonction permettant de changer l'information enregistré
	 * pour un membre ou d'un professionel dans un fichier
	 * en passant l'instance responsable de la gestion 
	 * du fichier en parametre
	 * */
    
    static void modification(int index, Csvio csv) {
       
    	String rep; //represente le champs a modifié
       
    	//Boucle infine pour changer un champs
        do {
        	
        	//Saisie du champs a modifier
            rep = Console.input("Voulez-vous changer\n "
                    + "(1) le nom\n"
                    + "(2) la date de naissance\n"
                    + "(3) l'adresse courriel\n"
                    + "(4) l`adresse postale\n"
                    + "(5) le numero de telephone\n"
                    + "(6) statut\n"
                    + "OU (7) quitter ");

            //Traitement specifique pour chaque champs
            switch(rep) {
                case "1":
                    String nom = Console.input("Nouveau Nom:");
                    csv.changer(nom, 0, index);
                    break;
                case "2":
                    String date = Console.input("Nouvelle Date de naissance");
                    csv.changer(date, 1, index);
                    break;
                case "3":
                    String courriel = Console.input("Nouvelle Adresse courriel:");
                    csv.changer(courriel, 2, index);
                    break;
                case "4":
                    String postale = Console.input("Nouvelle Adresse postale:");
                    csv.changer(postale, 3, index);
                    break;
                case "5":
                    String tel = Console.input("Nouveau Numero de telephone:");
                    csv.changer(tel, 4, index);
                    break;
                case "6":
                	Membre.valider(Integer.parseInt(csv.records.get(index).split(",")[6]), true);
					String statut = Console.input("Entre le nouveau statut, 1 ok, -1 frais non-paye, -2 banni autre raison");
                    csv.changer(statut, 5, index);
                    break;
                default: Console.output("Argument invalide"); break; 
            }
        } while(!rep.equals("7"));
    }

	/**
	 * Fonction permettant de supprimer un enregistrement 
	 * pour un membre ou d'un professionel dans un fichier
	 * en passant l'instance responsable de la gestion 
	 * du fichier en parametre et la ligne du fichier 
	 * qui devra etre supprimer
	 * 
	 * */
    
    public static void suppression(int index, Csvio csv) { csv.supprimer(index);}



}
