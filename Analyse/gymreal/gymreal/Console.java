package gymreal;

import java.io.IOException;
import java.util.Scanner;

/**
 * Class representant les methodes necessaire pour que l'interface
 * interagisse avec l'utilisateur
 */

public class Console {

	/**
	 * Methode de saisie sans aucune verification,
	 * qui affiche le message qui lui est passe
	 */
    static String input(String message) {
    	System.out.println(message);
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (s == "quit") {
            System.out.println("Vous avez bien quitté.");
            System.exit(0);
        }
        return s;
    }
    
	/**
	 * Methode de saisie qui verifie si l'élément saisie pourra etre
	 * parser, evite de mettre des try-catch a plusieur endroit dans le code
	 */
    static String input(String message, boolean check_integer) {
    	if(check_integer) {
    		do {
    			try {
        			System.out.println(message);
            		Scanner sc = new Scanner(System.in);
            		String s = sc.nextLine();
                	if (s.equals("quit")) {
                    	System.out.println("Vous avez bien quitté.");
                    	System.exit(0);
                	}
            		Integer.parseInt(s);
            		return s;
    			} catch(NumberFormatException e) {
    				System.out.println("Invalide entry");
    			}
    		} while(true);
    	}
    	else {
    		return input("");
    	}
    }
    
    /**
     * Methode d'affichage simple
     */
    static void output(String message) {
    	System.out.println(message);
    }
    
    
    /**
     * Methode pour effacer le contenue sur la page de la console,
     * ne fonctionne pas sur le terminal d'eclipse
     */
    static void clearScreen() {
    	try {
			Runtime.getRuntime().exec("clear");
		} catch (IOException e) {
			System.out.println("Unable to clear the screen");
		}
    }

   
}